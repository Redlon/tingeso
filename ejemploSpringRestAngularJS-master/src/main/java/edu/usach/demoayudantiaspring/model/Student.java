package edu.usach.demoayudantiaspring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long studentID;
    
    @NotNull
    @Size(min=2, max=30)
    private String name;
    
    private String rut;
    
    private String mail;
    
    private String carrera;
    
    private int ingreso;
    
    
    public long getStudentId() {
        return studentID;
    }

    public void setstudentId(long studentID) {
        this.studentID = studentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        rut = rut.toUpperCase();
        rut = rut.trim();
        this.rut = rut;
    }
    
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    
    public int getIngreso() {
        return ingreso;
    }

    public void setIngreso(int ingreso) {
        this.ingreso = ingreso;
    }
    
    
    public Student() {
    }
    
    
}
