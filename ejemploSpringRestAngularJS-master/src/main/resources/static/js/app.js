var app = angular.module('pokemon',['ngRoute']);

app.config(function($routeProvider){
    $routeProvider
        .when('/list',{
            templateUrl: 'js/views/studentsList.html',
            controller: 'StudentController'
        })
        .otherwise({
            redirectTo: '/index'
        });
});