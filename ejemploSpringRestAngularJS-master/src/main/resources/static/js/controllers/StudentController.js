app.controller('StudentController', ['$scope','$http', function($scope,$http) {
    $scope.students = [];
    $scope.newStudent = {};
    
    $scope.loadStudent = function(){
        $http.get('/students').then(function(response){
            console.log(response);
            
            $scope.students = response.data._embedded.students;
        });
    }
    
    $scope.send = function(){
        console.log("Ejecutando send");
        console.log($scope.newStudent);
        var student = {
            "name":$scope.newStudent.name,
            "rut":$scope.newStudent.rut,
            "mail":$scope.newStudent.mail,
            "carrera":$scope.newStudent.carrera,
            "ingreso":$scope.newStudent.ingreso
        };
        $http.post('/students',student).then(function(response){
            console.log(response);
            var studentUrl = response.data._links.student.href;
            $scope.loadStudent();
        });
    }
    
    $scope.loadStudent();    
}]);

