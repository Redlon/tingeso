app.controller('MainController', ['$scope','$location', function($scope,$location) {

	$scope.title = "Administracion de alumnos 2017-2";

        $scope.listPage = $location.path() === '/list';

	$scope.navigation = [
        {"text":"Ver estudiantes",   "link":"#!/list", "condition":$scope.listPage}
	];

}]);
