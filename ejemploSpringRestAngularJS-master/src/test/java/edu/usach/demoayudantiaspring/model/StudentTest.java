package edu.usach.demoayudantiaspring.model;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author marisel
 */
public class StudentTest {
    
    @Test
    public void formateoRuts(){
        
        Student s = new Student();
        
        s.setRut("  12.234.567-9    ");
        assertEquals(s.getRut(), "12.234.567-9");
        
        s.setRut("  12.234.567-k    ");
        assertEquals(s.getRut(), "12.234.567-K");

    }
    
 



}
